#include <prism/logging.h>
#include <iostream>
#include <cuda_runtime.h>

#define CUDA_CALL(func)                                      \
  {                                                          \
    cudaError_t e = (func);                                  \
    CHECK(e == cudaSuccess || e == cudaErrorCudartUnloading) \
        << "CUDA: " << cudaGetErrorString(e);                \
  }

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

__global__
void AddVector(float* z, float* x, float* y, int n) {
  size_t step = gridDim.x * blockDim.x;
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;

  for (size_t i = start; i < n; i += step)
    z[i] = x[i] + y[i];
}

int main() {
  int device_count = 0;
  CUDA_CALL(cudaGetDeviceCount(&device_count));
  CHECK_GT(device_count, 0);

  float* x = nullptr;
  CUDA_CALL(cudaMallocHost(&x, kArrayLength * sizeof(float)));
  float* y = nullptr;
  CUDA_CALL(cudaMallocHost(&y, kArrayLength * sizeof(float)));
  float* z = nullptr;
  CUDA_CALL(cudaMallocHost(&z, kArrayLength * sizeof(float)));
  CHECK(x && y && z) << "cudaMallocHost failed";

  LOG(INFO) << "cudaMallocHost finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  size_t offset = 0;
  size_t step = ROUND_UP(kArrayLength, device_count);
  for (int i = 0; i < device_count; i++) {
    CUDA_CALL(cudaSetDevice(i));
    if (offset + step > kArrayLength) step = kArrayLength - offset;
    size_t num_threads = 256;
    size_t num_blocks = ROUND_UP(step, num_threads);
    AddVector<<<num_blocks, num_threads>>>(z + offset, x + offset, y + offset, step);
    offset += step;
  }
  CUDA_CALL(cudaDeviceSynchronize());

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  CUDA_CALL(cudaFreeHost(x));
  CUDA_CALL(cudaFreeHost(y));
  CUDA_CALL(cudaFreeHost(z));
  return 0;
}

