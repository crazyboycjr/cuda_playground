#include <prism/logging.h>
#include <cuda_runtime.h>
#include <iostream>
#include <chrono>
#define CUDA_CALL(func)                                      \
  {                                                          \
    cudaError_t e = (func);                                  \
    CHECK(e == cudaSuccess || e == cudaErrorCudartUnloading) \
        << "CUDA: " << cudaGetErrorString(e);                \
  }

static __inline __attribute__((always_inline)) uint64_t rdtsc() {
#if defined(__i386__)
  int64_t ret;
  __asm__ volatile ("rdtsc" : "=A" (ret) );
  return ret;
#elif defined(__x86_64__) || defined(__amd64__)
  uint32_t lo, hi;
  __asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
  return (((uint64_t)hi << 32) | lo);
#elif defined(__aarch64__)
  //
  // arch/arm64/include/asm/arch_timer.h
  //
  // static inline u64 arch_counter_get_cntvct(void)
  // {
  //         u64 cval;
  // 
  //         isb();
  //         asm volatile("mrs %0, cntvct_el0" : "=r" (cval));
  // 
  //         return cval;
  // }
  //
  // https://github.com/cloudius-systems/osv/blob/master/arch/aarch64/arm-clock.cc
  uint64_t cntvct;
  asm volatile ("isb; mrs %0, cntvct_el0; isb; " : "=r" (cntvct) :: "memory");
  return cntvct;
#elif defined(__powerpc__) || defined (__powerpc64__)
  // Based on:
  // https://github.com/randombit/botan/blob/net.randombit.botan/src/lib/entropy/hres_timer/hres_timer.cpp
  uint32_t lo = 0, hi = 0;
  asm volatile("mftbu %0; mftb %1" : "=r" (hi), "=r" (lo));
  return (((uint64_t)hi << 32) | lo);
#else
#warning No high-precision counter available for your OS/arch
  return 0;
#endif
}

int main() {
  int count = 0;
  CUDA_CALL(cudaGetDeviceCount(&count));
  std::cout << "device count: " << count << std::endl;
  int device;
  CUDA_CALL(cudaGetDevice(&device));
  std::cout << "current device is: " << device << std::endl;
  char pci_bus_id[128];
  CUDA_CALL(cudaDeviceGetPCIBusId(pci_bus_id, 128, device));
  std::cout << "pci: " << pci_bus_id << std::endl;
  uint64_t c = 0;
  for (int i = 0; i < 100; i++) {
    // the results show that rdtsc is more stable than steady_clock
    //uint64_t a = rdtsc();
    //uint64_t b = rdtsc();
    auto a = std::chrono::steady_clock::now();
    auto b = std::chrono::steady_clock::now();
    c += (b - a).count();
  }
  std::cout << c / 100 << std::endl;
  return 0;
}
