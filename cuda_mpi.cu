#include <prism/logging.h>
#include <iostream>
#include <cuda_runtime.h>
#include <thread>
#include <chrono>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define MPI_CALL(cmd)                                                 \
  do {                                                                \
    int mpi_errno = cmd;                                              \
    if (MPI_SUCCESS != mpi_errno) {                                   \
      fprintf(stderr, "[%s:%d] MPI call failed with %d \n", __FILE__, \
              __LINE__, mpi_errno);                                   \
      exit(EXIT_FAILURE);                                             \
    }                                                                 \
    assert(MPI_SUCCESS == mpi_errno);                                 \
  } while (false)

#define CUDA_CALL(func)                                      \
  {                                                          \
    cudaError_t e = (func);                                  \
    CHECK(e == cudaSuccess || e == cudaErrorCudartUnloading) \
        << "CUDA: " << cudaGetErrorString(e);                \
  }

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

__global__
void AddVector(float* z, float* x, float* y, int n) {
  size_t step = gridDim.x * blockDim.x;
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;

  for (size_t i = start; i < n; i += step)
    z[i] = x[i] + y[i];
}

const int kWindow = 4;

void CudaCopy(cudaStream_t stream, void* from, void* to, size_t length) {
  while (1) {
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kWindow; i++) {
      CUDA_CALL(cudaMemcpyAsync(to, from, length, cudaMemcpyDeviceToHost, stream));
    }
    CUDA_CALL(cudaStreamSynchronize(stream));
    auto end = std::chrono::high_resolution_clock::now();
    printf("device to host throughput = %.2f MB/s\n", 1e3 * (length * kWindow) / (end - start).count());
  }
}

void MPISendRecv(void* send_buf, void* recv_buf, size_t length) {
  int world_rank;
  int world_size;
  MPI_Comm world_comm = MPI_COMM_WORLD;
  MPI_CALL(MPI_Comm_rank(world_comm, &world_rank));
  MPI_CALL(MPI_Comm_size(world_comm, &world_size));
  CHECK_EQ(world_size, 2);

  int dest_rank = world_rank ^ 1;
  int send_tag = world_rank;
  int recv_tag = world_rank ^ 1;
  MPI_Request sreq[kWindow];
  MPI_Request rreq[kWindow];
  for (int i = 0; i < kWindow; i++) {
    sreq[i] = rreq[i] = MPI_REQUEST_NULL;
  }

  while (1) {
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kWindow; i++) {
      MPI_CALL(MPI_Isend(send_buf, length, MPI_CHAR, dest_rank, send_tag, world_comm, &sreq[i]));
      MPI_CALL(MPI_Irecv(recv_buf, length, MPI_CHAR, dest_rank, recv_tag, world_comm, &rreq[i]));
    }
    MPI_CALL(MPI_Waitall(kWindow, sreq, MPI_STATUSES_IGNORE));
    MPI_CALL(MPI_Waitall(kWindow, rreq, MPI_STATUSES_IGNORE));
    auto end = std::chrono::high_resolution_clock::now();
    printf("2-way network throughput = %.2f MB/s\n", 1e3 * (length * kWindow) / (end - start).count());
  }
}

int main() {
  int required = MPI_THREAD_MULTIPLE;
  int provided = 0;
  MPI_CALL(MPI_Init_thread(nullptr, nullptr, required, &provided));
  CHECK_EQ(provided, required);

  float* device_buf = nullptr;
  CUDA_CALL(cudaMallocHost(&device_buf, kArrayLength * sizeof(float)));
  float* host_buf = static_cast<float*>(malloc(kArrayLength * sizeof(float)));

  CHECK(device_buf && host_buf);

  float* send_buf = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  float* recv_buf = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  CHECK(send_buf && recv_buf);

  LOG(INFO) << "cudaMallocHost finished";

  cudaStream_t stream;
  int greatest_priority;
  CUDA_CALL(cudaDeviceGetStreamPriorityRange(NULL, &greatest_priority));
  CUDA_CALL(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, greatest_priority));

  std::thread* copy_thread = new std::thread(CudaCopy, stream, device_buf, host_buf, kArrayLength * sizeof(float));
  std::thread* netio_thread = new std::thread(MPISendRecv, send_buf, recv_buf, kArrayLength * sizeof(float));

  copy_thread->join();
  netio_thread->join();
  delete copy_thread;
  delete netio_thread;

  CUDA_CALL(cudaFreeHost(device_buf));
  free(host_buf);
  free(send_buf);
  free(recv_buf);

  MPI_CALL(MPI_Finalize());
  return 0;
}

