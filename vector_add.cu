#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))

#define N 10000000
#define MAX_ERR 1e-6

__global__ void vector_add(float *out, float *a, float *b, int n) {
    for(int i = 0; i < n; i ++){
        out[i] = a[i] + b[i];
    }
}
__global__
void AddVector(float* z, float* x, float* y, int n) {
  size_t step = gridDim.x * blockDim.x;
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;

  for (size_t i = start; i < n; i += step)
    z[i] = x[i] + y[i];
}

int main(){
    float *a, *b, *out;
    float *d_a, *d_b, *d_out; 

    // Allocate host memory
    a   = (float*)malloc(sizeof(float) * N);
    b   = (float*)malloc(sizeof(float) * N);
    out = (float*)malloc(sizeof(float) * N);

    printf("malloc finished\n");

    // Initialize host arrays
    for(int i = 0; i < N; i++){
        a[i] = 1.0f;
        b[i] = 2.0f;
    }

    // Allocate device memory
    cudaMalloc((void**)&d_a, sizeof(float) * N);
    cudaMalloc((void**)&d_b, sizeof(float) * N);
    cudaMalloc((void**)&d_out, sizeof(float) * N);

    printf("cudaMalloc finished\n");

    int cnt = 0;
    // Transfer data from host to device memory
    printf("%d iteration copying\n", cnt);
    cudaMemcpy(d_a, a, sizeof(float) * N, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(float) * N, cudaMemcpyHostToDevice);
    printf("%d iteration copy finished\n", cnt);

    while (1) {
    // Executing kernel 
    size_t num_threads = 256;
    size_t num_blocks = ROUND_UP(N, num_threads);

      AddVector<<<num_blocks, num_threads>>>(d_out, d_a, d_b, N);
      //vector_add<<<1,1>>>(d_out, d_a, d_b, N);
    printf("%d iteration\n", cnt);
    }
    cudaMemcpy(out, d_out, sizeof(float) * N, cudaMemcpyDeviceToHost);
    
    // Transfer data back to host memory

    // Verification
    for(int i = 0; i < N; i++){
        assert(fabs(out[i] - a[i] - b[i]) < MAX_ERR);
    }
    printf("out[0] = %f\n", out[0]);
    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_out);

    // Deallocate host memory
    free(a); 
    free(b); 
    free(out);
}
