#include <prism/logging.h>
#include <iostream>
#include <cuda_runtime.h>
#include <cuda_fp16.h>
//#include <x86intrin.h>

#define CUDA_CALL(func)                                      \
  {                                                          \
    cudaError_t e = (func);                                  \
    CHECK(e == cudaSuccess || e == cudaErrorCudartUnloading) \
        << "CUDA: " << cudaGetErrorString(e);                \
  }

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

__global__
void AddVector(__half* z, __half* x, __half* y, int n) {
  size_t step = gridDim.x * blockDim.x;
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;
  //printf("%ld %ld\n", step, start);

  __half2* z2 = static_cast<__half2*>(static_cast<void*>(z));
  __half2* x2 = static_cast<__half2*>(static_cast<void*>(x));
  __half2* y2 = static_cast<__half2*>(static_cast<void*>(y));

  for (size_t i = start; i < n/2; i += step)
    z2[i] = __hadd2(x2[i], y2[i]);
  //for (size_t i = start; i < n; i += step)
  //  z[i] = __hadd(x[i], y[i]);
}

__global__
void InitVector(__half* x, __half* y, int n) {
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;
  size_t step = blockDim.x * gridDim.x;

  __half2* x2 = static_cast<__half2*>(static_cast<void*>(x));
  __half2* y2 = static_cast<__half2*>(static_cast<void*>(y));

  for (size_t i = start; i < n/2; i += step) {
    x2[i] = __float2half2_rn(1.0);
    y2[i] = __float2half2_rn(2.0);
  }
}

__global__
void CalcError(__half* z, int n, float* error) {
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;
  size_t step = blockDim.x * gridDim.x;

  for (size_t i = start; i < n; i += step) {
    *error += fabs(__half2float(z[i]) - 3.0);
  }
}

int main() {
  __half* x = nullptr;
  CUDA_CALL(cudaMallocHost(&x, kArrayLength * sizeof(__half)));
  __half* y = nullptr;
  CUDA_CALL(cudaMallocHost(&y, kArrayLength * sizeof(__half)));
  __half* z = nullptr;
  CUDA_CALL(cudaMallocHost(&z, kArrayLength * sizeof(__half)));
  CHECK(x && y && z) << "cudaMallocHost failed";

  LOG(INFO) << "cudaMallocHost finished";

  printf("%ld\n", sizeof(__half));
  printf("%ld\n", sizeof(__half2));
  printf("%ld\n", sizeof(float2));
  printf("%ld\n", sizeof(float3));
  printf("%ld\n", sizeof(float4));

  size_t num_threads = 256;
  size_t num_blocks = ROUND_UP(kArrayLength, num_threads);

#if (__CUDACC_VER_MAJOR__ == 9 && __CUDACC_VER_MINOR__ >= 2) || (__CUDACC_VER_MAJOR__ > 9)
  // the code can executed on __host__ after cuda 9.2
  for (int i = 0; i < kArrayLength; i++) {
    x[i] = __float2half_rn(1.0);
    y[i] = __float2half_rn(2.0);
    //x[i] = _cvtss_sh(1.0, 0);
    //y[i] = _cvtss_sh(2.0, 0);
  }

  printf("%.2x %.6f\n", x[0], __half2float(x[0]));
  printf("%.2x %.6f\n", x[1], __half2float(x[1]));
#else
  InitVector<<<num_blocks, num_threads>>>(x, y, kArrayLength);
  CUDA_CALL(cudaDeviceSynchronize());
#endif

  AddVector<<<num_blocks, num_threads>>>(z, x, y, kArrayLength);
  CUDA_CALL(cudaDeviceSynchronize());

#if (__CUDACC_VER_MAJOR__ == 9 && __CUDACC_VER_MINOR__ >= 2) || (__CUDACC_VER_MAJOR__ > 9)
  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(__half2float(z[i]) - 3.0);
    //error += fabs(_cvtsh_ss(z[i]) - 3.0);
  }
  LOG(INFO) << "error = " << error;
#else
  float* error = nullptr;
  CUDA_CALL(cudaMallocHost(&error, sizeof(float)));
  CalcError<<<num_blocks, num_threads>>>(z, kArrayLength, error);
  CUDA_CALL(cudaDeviceSynchronize());
  LOG(INFO) << "error = " << *error;
  CUDA_CALL(cudaFreeHost(error));
#endif

  CUDA_CALL(cudaFreeHost(x));
  CUDA_CALL(cudaFreeHost(y));
  CUDA_CALL(cudaFreeHost(z));
  return 0;
}

