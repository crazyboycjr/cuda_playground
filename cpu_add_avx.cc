#include <prism/logging.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <immintrin.h>

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB
static_assert(kArrayLength % 32 == 0, "kArrayLength is not multiply of 256bits");


#define CACHELINE_SIZE_BYTES      (64u)
#define CACHELINE_SIZE_FLOATS     (CACHELINE_SIZE_BYTES/sizeof(float))
#define CACHELINE_SIZE_BYTE_MASK  (CACHELINE_SIZE_BYTES-1)
#define CACHELINE_SIZE_FLOAT_MASK (CACHELINE_SIZE_FLOATS-1)

#define INSTRUCTION_VECTOR_SIZE           (CACHELINE_SIZE_FLOATS)
#define INSTRUCTION_VECTOR_SIZE_MASK      (CACHELINE_SIZE_FLOATS-1)
#define INSTRUCTION_VECTOR_SIZE_ADDR_MASK (CACHELINE_SIZE_BYTES-1)


class Aggregator
{
public:
	//static Aggregator* Create(std::string name, const size_t prefetch_dist);

	//add vector 2 (src) to vector 1 (dst)
	virtual void VectorVectorAdd(float* dst, size_t len, float* src)
	{
		assert((len & INSTRUCTION_VECTOR_SIZE_MASK) == 0);
		for (size_t m = 0; m < len; m += INSTRUCTION_VECTOR_SIZE) {
			_mm_store_ps(dst + m, _mm_add_ps(_mm_load_ps(dst + m), _mm_load_ps(src + m)));
		}
	}

public:
	const size_t prefetch_distance;

	Aggregator(const size_t prefetch_distance = 0x240)
		: prefetch_distance(prefetch_distance)
	{ }

	virtual ~Aggregator() { }
};

class TTAggregator : public Aggregator
{
public:
	TTAggregator(const size_t prefetch_distance = 0x240)
		: Aggregator(prefetch_distance)
	{ }

	virtual void VectorVectorAdd(float* dst, size_t len, float* src)
	{
		assert((len & INSTRUCTION_VECTOR_SIZE_MASK) == 0);
		size_t offset = 0;
		size_t prefetch_offset = prefetch_distance;
		__asm__ __volatile__(".align 32 \n\t"
			"1: \n\t"

			"movaps 0x00(%[dst],%[offset],1),%%xmm0 \n\t"
			"movaps 0x10(%[dst],%[offset],1),%%xmm1 \n\t"
			"movaps 0x20(%[dst],%[offset],1),%%xmm2 \n\t"
			"movaps 0x30(%[dst],%[offset],1),%%xmm3 \n\t"

			"addps  0x00(%[src],%[offset],1),%%xmm0 \n\t"
			"addps  0x10(%[src],%[offset],1),%%xmm1 \n\t"
			"addps  0x20(%[src],%[offset],1),%%xmm2 \n\t"
			"addps  0x30(%[src],%[offset],1),%%xmm3 \n\t"

			"movaps  %%xmm0,0x00(%[dst],%[offset],1) \n\t"
			"movaps  %%xmm1,0x10(%[dst],%[offset],1) \n\t"
			"movaps  %%xmm2,0x20(%[dst],%[offset],1) \n\t"
			"movaps  %%xmm3,0x30(%[dst],%[offset],1) \n\t"

			"add %[inc],%[offset] \n\t"

			"cmp %[end], %[offset] \n\t"
			"jb 1b \n\t"

			"sfence \n\t"

			: [offset] "+mr" (offset),
			[prefetch_offset] "+mr" (prefetch_offset)
			: [dst] "c" (dst), // Use rcx to keep insns small
			[src] "D" (src), // Use rdi to keep insns small
			[inc] "i" (CACHELINE_SIZE_BYTES),
			[end] "mr" (len * sizeof(float))
			: "cc", "memory",
			"xmm0", "xmm1", "xmm2", "xmm3",
			"xmm4", "xmm5", "xmm6", "xmm7");
	}
};


// memory bounded, thus no speed up
void AddVector(float* z, float* x, float* y, int n) {
  // we use avx2
  for (int i = 0; i < n; i += 8) {
    __m256 vx = _mm256_load_ps(x + i);
    __m256 vy = _mm256_load_ps(y + i);
    __m256 vz = _mm256_add_ps(vx, vy);
    _mm256_store_ps(z + i, vz);
    //__m256* vx = reinterpret_cast<__m256*>(x + i);
    //__m256* vy = reinterpret_cast<__m256*>(y + i);
    //__m256* vz = reinterpret_cast<__m256*>(z + i);
    //*vz = _mm256_add_ps(*vx, *vy);
    //z[i] = x[i] + y[i];
  }
}

void PrintTime(uint64_t ltime_ns) {
  std::string unit = "ns";
  double time_ns = ltime_ns;
  if (time_ns >= 1000) time_ns /= 1000, unit = "us";
  if (time_ns >= 1000) time_ns /= 1000, unit = "ms";
  if (time_ns >= 1000) time_ns /= 1000, unit = "s";
  std::cout << "Time: " << time_ns << unit << std::endl;
}

int main() {
  float* x = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  float* y = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  float* z = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  CHECK(x && y && z) << "malloc failed";

  LOG(INFO) << "malloc finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  auto start = std::chrono::high_resolution_clock::now();

  //size_t num_threads = 1;
  //size_t num_blocks = ROUND_UP(kArrayLength, num_threads);
  auto aggregator = std::make_shared<TTAggregator>();
  //auto aggregator = std::make_shared<Aggregator>();
  //AddVector(z, x, y, kArrayLength);
  aggregator->VectorVectorAdd(z, kArrayLength, x);
  aggregator->VectorVectorAdd(z, kArrayLength, y);

  auto end = std::chrono::high_resolution_clock::now();

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  PrintTime((end - start).count());

  free(x); free(y); free(z);
  return 0;
}

