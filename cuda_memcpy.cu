#include <prism/logging.h>
#include <iostream>
#include <cuda_runtime.h>
#include <thread>
#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define CUDA_CALL(func)                                      \
  {                                                          \
    cudaError_t e = (func);                                  \
    CHECK(e == cudaSuccess || e == cudaErrorCudartUnloading) \
        << "CUDA: " << cudaGetErrorString(e);                \
  }

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

__global__
void AddVector(float* z, float* x, float* y, int n) {
  size_t step = gridDim.x * blockDim.x;
  size_t start = blockIdx.x * blockDim.x + threadIdx.x;

  for (size_t i = start; i < n; i += step)
    z[i] = x[i] + y[i];
}

const int kWindow = 4;

void CudaCopy(int tid, cudaStream_t stream, void* from, void* to, size_t length) {
  while (1) {
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kWindow; i++) {
      CUDA_CALL(cudaMemcpyAsync(to, from, length, cudaMemcpyDeviceToHost, stream));
    }
    CUDA_CALL(cudaStreamSynchronize(stream));
    auto end = std::chrono::high_resolution_clock::now();
    printf("[%d]: device to host throughput = %.2f MB/s\n", tid, 1e3 * (length * kWindow) / (end - start).count());
  }
}

int main() {
  int device_count = 0;
  CUDA_CALL(cudaGetDeviceCount(&device_count));
  std::cout << "device count: " << device_count << std::endl;

  float* device_buf[device_count];
  float* host_buf[device_count];

  for (int i = 0;i < device_count; i++) {
    CUDA_CALL(cudaSetDevice(i));
    CUDA_CALL(cudaMalloc(&device_buf[i], kArrayLength * sizeof(float)));
    CUDA_CALL(cudaMallocHost(&host_buf[i], kArrayLength * sizeof(float)));
    //host_buf[i] = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
    CHECK(device_buf[i] && host_buf[i]);
  }


  LOG(INFO) << "cudaMallocHost finished";

  cudaStream_t stream[device_count];
  int greatest_priority;
  CUDA_CALL(cudaDeviceGetStreamPriorityRange(NULL, &greatest_priority));
  for (int i = 0; i < device_count; i++) {
    CUDA_CALL(cudaSetDevice(i));
    CUDA_CALL(cudaStreamCreateWithPriority(&stream[i], cudaStreamNonBlocking, greatest_priority));
  }

  std::thread* copy_thread[device_count];
  for (int i = 0; i < device_count; i++) {
    copy_thread[i] = new std::thread(CudaCopy, i, stream[i], device_buf[i], host_buf[i], kArrayLength * sizeof(float));
  }

  for (int i = 0; i < device_count; i++) {
    copy_thread[i]->join();
    delete copy_thread[i];
  }

  for (int i = 0; i < device_count; i++) {
    CUDA_CALL(cudaFreeHost(device_buf[i]));
    free(host_buf[i]);
  }

  return 0;
}

