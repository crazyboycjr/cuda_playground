#include <prism/logging.h>
#include <iostream>
#include <chrono>
#include <cmath>

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

void AddVector(float* z, float* x, float* y, int n) {
  for (int i = 0; i < n; i++)
    z[i] = x[i] + y[i];
}

void PrintTime(uint64_t ltime_ns) {
  std::string unit = "ns";
  double time_ns = ltime_ns;
  if (time_ns >= 1000) time_ns /= 1000, unit = "us";
  if (time_ns >= 1000) time_ns /= 1000, unit = "ms";
  if (time_ns >= 1000) time_ns /= 1000, unit = "s";
  std::cout << "Time: " << time_ns << unit << std::endl;
}

int main() {
  float* x = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  float* y = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  float* z = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  CHECK(x && y && z) << "malloc failed";

  LOG(INFO) << "malloc finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  auto start = std::chrono::high_resolution_clock::now();

  //size_t num_threads = 1;
  //size_t num_blocks = ROUND_UP(kArrayLength, num_threads);
  AddVector(z, x, y, kArrayLength);

  auto end = std::chrono::high_resolution_clock::now();

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  PrintTime((end - start).count());

  free(x); free(y); free(z);
  return 0;
}

