#include <prism/logging.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <thread>
#include <omp.h>
#include <type_traits>

enum DeviceType {
  CPUDevice,
  CPUDeviceSingle,
  CPUDeviceThread,
  GPUDevice,
};

template <DeviceType Type, typename T>
class Accumulator {};

template <typename T>
class Accumulator<CPUDeviceSingle, T> {
 public:
  void operator()(std::vector<std::vector<T>>& src) {
    std::vector<T>& dst = src[0];
    size_t len = dst.size();
    for (size_t i = 0; i < len; i++) {
      for (size_t j = 1; j < src.size(); j++) dst[i] += src[j][i];
    }
  }
};

template <typename SubType>
class Expr {
 public:
  const SubType& self() const { return *static_cast<const SubType*>(this); }
};

template <typename LType, typename RType>
struct AddOp {
  using ltype = typename LType::type;
  using rtype = typename RType::type;
  using type = typename std::common_type<ltype, rtype>::type;
  static type Map(ltype a, rtype b) { return a + b; }
};

template <typename Op, typename LType, typename RType>
struct BinaryExpr : public Expr<BinaryExpr<Op, LType, RType>> {
  using ltype = typename LType::type;
  using rtype = typename RType::type;
  using type = typename std::common_type<ltype, rtype>::type;
  const LType& lhs;
  const RType& rhs;
  BinaryExpr(const LType& lhs, const RType& rhs) : lhs(lhs), rhs(rhs) {}
  type Eval(int i) const { return Op::Map(lhs.Eval(i), rhs.Eval(i)); }
};

template <typename T>
class Vector : public Expr<Vector<T>> {
 public:
  using type = T;
  Vector() {}
  ~Vector() {}
  explicit Vector(T* dptr, size_t len) : dptr_(dptr), len_(len) {}
  template <typename DType>
  Vector& operator=(const Expr<DType>& expr) {
    const DType& e = expr.self();
    for (size_t i = 0; i < len_; i++) {
      dptr_[i] = e.Eval(i);
    }
    return *this;
  }
  template <typename DType>
  Vector& operator+=(const Expr<DType>& expr) {
    const DType& e = expr.self();
    for (size_t i = 0; i < len_; i++) {
      dptr_[i] += e.Eval(i);
    }
    return *this;
  }

  T Eval(int i) const { return dptr_[i]; }

 private:
  T* dptr_;
  size_t len_;
};

template <typename Op, typename LType, typename RType>
inline BinaryExpr<Op, LType, RType> BinaryOp(const Expr<LType>& lhs,
                                             const Expr<RType>& rhs) {
  return BinaryExpr<Op, LType, RType>(lhs.self(), rhs.self());
}

template <typename LType, typename RType>
inline BinaryExpr<AddOp<LType, RType>, LType, RType> operator+(
    const Expr<LType>& lhs, const Expr<RType>& rhs) {
  return BinaryOp<AddOp<LType, RType>>(lhs, rhs);
}

template <typename T>
class Accumulator<CPUDeviceThread, T> {
 public:
  void ReduceSum(std::vector<std::vector<T>>& src, size_t offset, size_t len) {
    // TODO(cjr): remove the extra copy of valarray, like some tech used in
    // Eigen
    auto& dst = src[0];
    Vector<T> out0(dst.data() + offset, len);
    for (size_t i = 1; i < src.size(); i += 4) {
      switch (src.size() - i) {
        case 1: {
          Vector<T> in1(src[i].data() + offset, len);
          out0 += in1;
          break;
        }
        case 2: {
          Vector<T> in1(src[i].data() + offset, len);
          Vector<T> in2(src[i + 1].data() + offset, len);
          out0 += in1 + in2;
          break;
        }
        case 3: {
          Vector<T> in1(src[i].data() + offset, len);
          Vector<T> in2(src[i + 1].data() + offset, len);
          Vector<T> in3(src[i + 2].data() + offset, len);
          out0 += in1 + in2 + in3;
          break;
        }
        default: {
          Vector<T> in1(src[i].data() + offset, len);
          Vector<T> in2(src[i + 1].data() + offset, len);
          Vector<T> in3(src[i + 2].data() + offset, len);
          Vector<T> in4(src[i + 3].data() + offset, len);
          out0 += in1 + in2 + in3 + in4;
          break;
        }
      }
    }
    // std::copy(std::begin(out0), std::end(out0), std::begin(dst) + offset);
  }

  void operator()(std::vector<std::vector<T>>& src) {
    auto& dst = src[0];
    size_t len = dst.size();
    const size_t step = 4096;
    size_t ntask = (len + step - 1) / step;
    int nthread_reduction = std::thread::hardware_concurrency();
    if (len < step || nthread_reduction <= 1) {
      ReduceSum(src, 0, len);
    } else {
#pragma omp parallel for schedule(static) num_threads(nthread_reduction)
      for (size_t j = 0; j < ntask; j++) {
        size_t begin = std::min(j * step, len);
        size_t end = std::min((j + 1) * step, len);
        ReduceSum(src, begin, end - begin);
      }
    }
  }
};

template <typename T>
class Accumulator<CPUDevice, T> {
 public:
  void operator()(std::vector<std::vector<T>>& src) {
    Accumulator<CPUDeviceThread, T> ac;
    // Accumulator<CPUDeviceSingle, T> ac;
    ac(src);
  }
};

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

void AddVector(float* z, float* x, float* y, int n) {
  for (int i = 0; i < n; i++)
    z[i] = x[i] + y[i];
}

void PrintTime(uint64_t ltime_ns) {
  std::string unit = "ns";
  double time_ns = ltime_ns;
  if (time_ns >= 1000) time_ns /= 1000, unit = "us";
  if (time_ns >= 1000) time_ns /= 1000, unit = "ms";
  if (time_ns >= 1000) time_ns /= 1000, unit = "s";
  std::cout << "Time: " << time_ns << unit << std::endl;
}

int main() {
  std::vector<std::vector<float>> v;
  v.emplace_back(std::vector<float>(kArrayLength));
  v.emplace_back(std::vector<float>(kArrayLength));
  v.emplace_back(std::vector<float>(kArrayLength));

  std::vector<float>& z = v[0];
  std::vector<float>& x = v[1];
  std::vector<float>& y = v[2];

  LOG(INFO) << "vector created finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  auto start = std::chrono::high_resolution_clock::now();

  Accumulator<CPUDevice, float> ac;
  ac(v);

  auto end = std::chrono::high_resolution_clock::now();

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  PrintTime((end - start).count());
  return 0;
}

