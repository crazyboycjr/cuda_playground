.PHONY: clean

CXX ?= g++
NVCC ?= nvcc

#SRCS := $(shell find . -name "*.c[cu]" -type f)
#OBJS := $(patsubst %.cc,%,$(filter %.cc,$(SRCS))) $(patsubst %.cu,%,$(filter %.cu,$(SRCS)))
CCSRCS := $(shell find . -name "*.cc" -type f)
CUSRCS := $(shell find . -name "*.cu" -type f)
OBJS := $(patsubst %.cc,%,$(CCSRCS)) $(patsubst %.cu,%,$(CUSRCS))

all: $(OBJS)

run:
	echo $(OBJS)

cpu_add: cpu_add.cc
	$(CXX) $^ -o $@ -O3 -Wall -I.

cpu_add_thread_stride: cpu_add_thread_stride.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -lpthread

cpu_add_thread_block: cpu_add_thread_block.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -lpthread

cpu_add_avx2: cpu_add_avx2.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -march=native

cpu_add_avx: cpu_add_avx.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -march=native

cpu_add_avx2_thread: cpu_add_avx2_thread.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -march=native -lpthread -fopenmp

cpu_add_tmpl_expr: cpu_add_tmpl_expr.cc
	$(CXX) $^ -o $@ -O3 -Wall -I. -fopenmp

cuda_add: cuda_add.cu
	$(NVCC) $^ -o $@ -O3 -I.

cuda_add_md: cuda_add_md.cu
	$(NVCC) $^ -o $@ -O3 -I.

cuda_get_device: cuda_get_device.cu
	$(NVCC) $^ -o $@ -O3 -I.

# -arch=sm_70 for Tesla V100
# -arch=sm_50 for my GTX 960m
# if the generated code version is higher than the real gpu architecture
# the cuda kernel will not launch and no error be repoted
cuda_add_fp16: cuda_add_fp16.cu
	$(NVCC) $^ -o $@ -O3 -I. -arch=sm_70


clean:
	rm -rfv $(OBJS)
