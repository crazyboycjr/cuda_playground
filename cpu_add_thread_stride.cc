#include <prism/logging.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <thread>

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB

void AddVector(float* z, float* x, float* y, int n, int step = 1) {
  for (int i = 0; i < n; i += step)
    z[i] = x[i] + y[i];
}

void PrintTime(uint64_t ltime_ns) {
  std::string unit = "ns";
  double time_ns = ltime_ns;
  if (time_ns >= 1000) time_ns /= 1000, unit = "us";
  if (time_ns >= 1000) time_ns /= 1000, unit = "ms";
  if (time_ns >= 1000) time_ns /= 1000, unit = "s";
  std::cout << "Time: " << time_ns << unit << std::endl;
}

int main() {
  float* x = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  float* y = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  float* z = static_cast<float*>(malloc(kArrayLength * sizeof(float)));
  CHECK(x && y && z) << "malloc failed";

  LOG(INFO) << "malloc finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  int num_threads = std::thread::hardware_concurrency();
  LOG(INFO) << "hardware_concurrency: " << num_threads << std::endl;

  auto start = std::chrono::high_resolution_clock::now();

  //size_t step = ROUND_UP(kArrayLength, num_threads);
  std::vector<std::thread*> ths;
  for (int i = 0; i < num_threads; i++) {
    //AddVector(z + i, x + i, y + i, n, step);
    ths.emplace_back(new std::thread(AddVector, z + i, x + i, y + i, kArrayLength, num_threads));
  }

  for (auto th : ths) th->join();

  auto end = std::chrono::high_resolution_clock::now();

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  PrintTime((end - start).count());

  free(x); free(y); free(z);
  return 0;
}

