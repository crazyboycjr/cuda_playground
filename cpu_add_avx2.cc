#include <prism/logging.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <immintrin.h>

#define ROUND_UP(x, n) (((x) + (n)-1) / (n))
const int kArrayLength = 32 * 1048576;  // 32MB
static_assert(kArrayLength % 32 == 0, "kArrayLength is not multiply of 256bits");

// memory bounded, thus no speed up
void AddVector(float* z, float* x, float* y, int n) {
  // we use avx2
  for (int i = 0; i < n; i += 8) {
    __m256 vx = _mm256_load_ps(x + i);
    __m256 vy = _mm256_load_ps(y + i);
    __m256 vz = _mm256_add_ps(vx, vy);
    _mm256_store_ps(z + i, vz);
    //__m256* vx = reinterpret_cast<__m256*>(x + i);
    //__m256* vy = reinterpret_cast<__m256*>(y + i);
    //__m256* vz = reinterpret_cast<__m256*>(z + i);
    //*vz = _mm256_add_ps(*vx, *vy);
    //z[i] = x[i] + y[i];
  }
}

void PrintTime(uint64_t ltime_ns) {
  std::string unit = "ns";
  double time_ns = ltime_ns;
  if (time_ns >= 1000) time_ns /= 1000, unit = "us";
  if (time_ns >= 1000) time_ns /= 1000, unit = "ms";
  if (time_ns >= 1000) time_ns /= 1000, unit = "s";
  std::cout << "Time: " << time_ns << unit << std::endl;
}

int main() {
  float* x = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  float* y = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  float* z = static_cast<float*>(aligned_alloc(32, kArrayLength * sizeof(float)));
  CHECK(x && y && z) << "malloc failed";

  LOG(INFO) << "malloc finished";

  for (int i = 0; i < kArrayLength; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  auto start = std::chrono::high_resolution_clock::now();

  //size_t num_threads = 1;
  //size_t num_blocks = ROUND_UP(kArrayLength, num_threads);
  AddVector(z, x, y, kArrayLength);

  auto end = std::chrono::high_resolution_clock::now();

  float error = 0;
  for (int i = 0; i < kArrayLength; i++) {
    error += fabs(z[i] - 3.0);
  }

  LOG(INFO) << "error = " << error;

  PrintTime((end - start).count());

  free(x); free(y); free(z);
  return 0;
}

